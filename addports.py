import sys
import json
import re
import os.path

OPENED_HEAD = '=== START SUCCESSFUL SCANS ==='
OPENED_TAIL = '=== END SUCCESSFUL SCANS ==='

CLOSED_HEAD = '=== START FAILED SCANS ==='
CLOSED_TAIL = '=== END FAILED SCANS ==='

def load_db(filename):
    if not os.path.isfile(filename):
        with open(filename, 'w') as f:
            f.write(json.dumps({'opened': [], 'closed': []}))
    with open(filename, 'r') as f:
        return json.loads(f.read())

def save_db(filename, data):
    with open(filename, 'w') as f:
        data['opened'] = list(set(data['opened']))
        data['closed'] = list(set(data['closed']))
        data['opened'].sort()
        data['closed'].sort()
        f.write(json.dumps(data))

def parse_output(db, output):
    if OPENED_HEAD in output:
        opened = output[output.find(OPENED_HEAD) + len(OPENED_HEAD):output.find(OPENED_TAIL)]
        db['opened'] += [int(s) for s in re.compile(r'\s+').split(opened) if s != '']
    elif CLOSED_HEAD in output:
        closed = output[output.find(CLOSED_HEAD) + len(CLOSED_HEAD):output.find(CLOSED_TAIL)]
        db['closed'] += [int(s) for s in re.compile(r'\s+').split(closed) if s != '']

def load_output(filename):
    with open(filename, 'r') as f:
        return f.read()

def main():
    filename = input('Port database filename: ')
    db = load_db(filename)
    output = load_output(input('openportscanner output file: '))
    parse_output(db, output)
    save_db(filename, db)

if __name__ == '__main__':
    main()
