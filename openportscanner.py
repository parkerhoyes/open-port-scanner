import socket
import sys
import threading
import time
import hashlib

ACKNOWLEDGE = b'\x8d\xb2\xb6\xdb\xe7\xb4\xe6\xee'

LOG_INFO = 'LOG_INFO'
LOG_WARN = 'LOG_WARN'

# The time delay after which, if no expected data was received, a control connection is considered to have failed
CONTROL_TIMEOUT = 10
# The time delay after which, if no expected data was received, a scan connection is considered to have failed (or
# failed to establish)
SCAN_TIMEOUT = 1

# The maximum time for which the control connection can idle before the connection should be terminated
CONTROL_IDLE_TIMEOUT = 20

BACKLOG = 10

def int16_to_bytes(n):
    return bytes([(n >> 8) & 0xFF, n & 0xFF])

def bytes_to_int16(b):
    return (b[0] << 8) | b[1]

def log(severity, msg):
    print('[' + {LOG_INFO: 'INFO', LOG_WARN: 'WARN'}[severity] + '] ' + msg)

class ScannerServer:
    def __init__(self, port, address='0.0.0.0'):
        self.port = port
        self.address = address
    def start(self):
        s = socket.socket()
        s.bind((self.address, self.port))
        s.settimeout(None)
        s.listen(BACKLOG)
        while True:
            log(LOG_INFO, 'Waiting for control connection...')
            c, addr = s.accept()
            log(LOG_INFO, 'Accepted control connection from ' + addr[0] + ':' + str(addr[1]))
            c.settimeout(CONTROL_TIMEOUT)
            try:
                ack = b''
                while len(ack) < len(ACKNOWLEDGE) + 2:
                    ack += c.recv(len(ACKNOWLEDGE) + 2 - len(ack))
            except socket.error:
                c.close()
                log(LOG_WARN, 'Control connection failed; control connection closed')
                continue
            if ack != ACKNOWLEDGE + b'\x00\x00':
                c.close()
                log(LOG_WARN, 'Control client sent invalid acknowledgement packet; control connection closed')
                continue
            del ack
            try:
                c.send(ACKNOWLEDGE + b'\x00\x00')
            except socket.error:
                c.close()
                log(LOG_WARN, 'Control connection failed; control connection closed')
                continue
            log(LOG_INFO, 'Control connection established')
            self._control_loop(c)
            c.close()
            log(LOG_INFO, 'Control connection closed; restarting control loop')
    def _control_loop(self, control_client):
        while True:
            try:
                buff = b''
                start = time.time()
                end_routine = False
                while len(buff) < 2:
                    if time.time() - start > CONTROL_IDLE_TIMEOUT:
                        control_client.close()
                        log(LOG_WARN, 'Control client reached idle timeout; control connection closed')
                        end_routine = True
                        break
                    buff += control_client.recv(2 - len(buff))
                if end_routine:
                    break
                del end_routine
            except socket.error:
                control_client.close()
                log(LOG_WARN, 'Control connection failed; control connection closed')
                break
            port = bytes_to_int16(buff)
            del buff
            scan_server = socket.socket()
            scan_server.settimeout(SCAN_TIMEOUT)
            try:
                scan_server.bind((self.address, port))
            except socket.error:
                scan_server.close()
                try:
                    control_client.send(b'\x00')
                except socket.error:
                    control_client.close()
                    log(LOG_WARN, 'Control connection failed; control connection closed')
                    break
                log(LOG_WARN, 'Failed to bind to scan port; control client notified')
                continue
            scan_server.listen(BACKLOG)
            try:
                control_client.send(b'\x01')
            except socket.error:
                control_client.close()
                log(LOG_WARN, 'Control connection failed; control connection closed')
                break
            log(LOG_INFO, 'Waiting for scan connection on port ' + str(port) + '...')
            try:
                scan_client, scan_addr = scan_server.accept()
            except socket.timeout:
                scan_server.close()
                log(LOG_INFO, 'Scan server listen timeout; scan server closed')
                continue
            log(LOG_INFO, 'Accepted scan connection from ' + scan_addr[0] + ':' + str(scan_addr[1]))
            scan_client.settimeout(SCAN_TIMEOUT)
            try:
                ack = b''
                start = time.time()
                while len(ack) < len(ACKNOWLEDGE) + 2:
                    if time.time() - start > SCAN_TIMEOUT:
                        scan_client.close()
                        log(LOG_INFO, 'Scan client timed out; scan connection closed')
                        continue
                    ack += scan_client.recv(len(ACKNOWLEDGE) + 2 - len(ack))
            except socket.error:
                scan_client.close()
                log(LOG_INFO, 'Scan connection failed; scan connection closed')
                continue
            if ack != ACKNOWLEDGE + int16_to_bytes(port):
                scan_client.close()
                log(LOG_WARN, 'Scan client sent invalid acknowledge packet; scan connection closed')
                continue
            del ack
            try:
                scan_client.send(ACKNOWLEDGE + int16_to_bytes(port))
            except socket.error:
                scan_client.close()
                log(LOG_INFO, 'Scan connection failed; scan connection closed')
                continue
            scan_client.close()
            log(LOG_INFO, 'Successfully completed scan routine; scan connection closed')

class ScannerClient:
    SCAN_ERROR = 'SCAN_ERROR'
    SCAN_SUCCESS = 'SCAN_SUCCESS'
    SCAN_FAILURE = 'SCAN_FAILURE'
    SCAN_CANNOT_COMPLY = 'SCAN_CANNOT_COMPLY'
    def __init__(self, address, port):
        self.address = address
        self.port = port
        self.socket = None
    def connect(self):
        s = socket.socket()
        s.settimeout(CONTROL_TIMEOUT)
        log(LOG_INFO, 'Connecting to control server...')
        try:
            s.connect((self.address, self.port))
        except socket.error:
            s.close()
            log(LOG_WARN, 'Failed to connect to server')
            return
        try:
            s.send(ACKNOWLEDGE + b'\x00\x00')
        except socket.error:
            s.close()
            log(LOG_WARN, 'Control connection failed; control connection closed')
            return
        try:
            ack = b''
            while len(ack) < len(ACKNOWLEDGE) + 2:
                ack += s.recv(len(ACKNOWLEDGE) + 2 - len(ack))
        except socket.error:
            s.close()
            log(LOG_WARN, 'Control connection failed; control connection closed')
            return
        if ack != ACKNOWLEDGE + b'\x00\x00':
            s.close()
            log(LOG_WARN, 'Control server sent invalid acknowledge packet; control connection closed')
            return
        del ack
        self.socket = s
    def test(self, port):
        control_server = self.socket
        try:
            control_server.send(int16_to_bytes(port))
            log(LOG_INFO, 'Sent control server request to scan port ' + str(port))
            n = b''
            start = time.time()
            while n == b'':
                if time.time() - start > CONTROL_TIMEOUT:
                    control_server.close()
                    log(LOG_WARN, 'Control connection timed out; control connection closed')
                    return ScannerClient.SCAN_ERROR
                n = control_server.recv(1)
        except socket.error:
            control_server.close()
            log(LOG_WARN, 'Control connection failed; control connection closed')
            return ScannerClient.SCAN_ERROR
        if n == b'\x00':
            log(LOG_INFO, 'Scan server replied that it could not comply with request; scan routine ended')
            return ScannerClient.SCAN_CANNOT_COMPLY
        del n
        scan_server = socket.socket()
        scan_server.settimeout(SCAN_TIMEOUT)
        try:
            log(LOG_INFO, 'Connecting to scan server...')
            scan_server.connect((self.address, port))
        except socket.error:
            scan_server.close()
            log(LOG_INFO, 'Failed to connect to scan server; scan routine ended')
            return ScannerClient.SCAN_FAILURE
        try:
            scan_server.send(ACKNOWLEDGE + int16_to_bytes(port))
            ack = b''
            start = time.time()
            while len(ack) < len(ACKNOWLEDGE) + 2:
                if time.time() - start > SCAN_TIMEOUT:
                    scan_server.close()
                    log(LOG_INFO, 'Scan connection timed out; scan routine ended')
                    return ScannerClient.SCAN_FAILURE
                ack += scan_server.recv(len(ACKNOWLEDGE) + 2 - len(ack))
        except socket.error:
            scan_server.close()
            log(LOG_INFO, 'Scan connection failed; scan rountine ended')
            return ScannerClient.SCAN_FAILURE
        if ack != ACKNOWLEDGE + int16_to_bytes(port):
            scan_server.close()
            log(LOG_WARN, 'Scan server sent invalid acknowledgement packet; scan connection closed')
            return ScannerClient.SCAN_FAILURE
        del ack
        scan_server.close()
        log(LOG_INFO, 'Scan rountine completed successfully; scan connection closed')
        return ScannerClient.SCAN_SUCCESS
    def testall(self, ports):
        results = {}
        for port in ports:
            result = self.test(port)
            if result is ScannerClient.SCAN_ERROR:
                return 'error', results
            results[port] = result
        return 'success', results
    def close(self):
        self.socket.close()

def _getports(s):
    ports = []
    for item in s.split(','):
        if item.count('-') == 0:
            ports.append(int(item))
        elif item.count('-') == 1:
            start, end = item.split('-')
            start = int(start)
            end = int(end)
            if end < start:
                raise ValueError()
            ports += range(start, end + 1)
        else:
            raise ValueError()
    return ports

def _format_port_list(l):
    l.sort()
    lines = ['']
    for p in l:
        if len(lines[-1]) > 74:
            lines.append('')
        ps = str(p)
        lines[-1] = lines[-1] + ' ' * (7 - len(ps)) + ps
    lines.append('')
    return '\n'.join(lines)

def _main():
    if len(sys.argv) < 2:
        return False
    if sys.argv[1] == 'server':
        address = '0.0.0.0'
        if len(sys.argv) != 3:
            return False
        if len(sys.argv[2].split(':')) != 2:
            try:
                port = int(sys.argv[2])
            except ValueError:
                return False
        else:
            try:
                port = int(sys.argv[2].split(':')[1])
            except ValueError:
                return False
            address = sys.argv[2].split(':')[0]
        server = ScannerServer(port, address)
        server.start()
    elif sys.argv[1] == 'client':
        if len(sys.argv) != 4:
            return False
        if len(sys.argv[2].split(':')) != 2:
            return False
        address, port = sys.argv[2].split(':')
        try:
            port = int(port)
        except ValueError:
            return False
        try:
            ports = _getports(sys.argv[3])
        except ValueError:
            return False
        client = ScannerClient(address, port)
        client.connect()
        status, results = client.testall(ports)
        if status == 'success':
            print('Scan completed successfully.')
        else:
            print('Scan encountered an error and was unable to scan all ports (failed while\nscanning port ' +\
                    str(ports[ports.index(max(results)) + 1]) + ').')
        successful_scans = [i for i in results if results[i] is ScannerClient.SCAN_SUCCESS]
        failed_scans = [i for i in results if results[i] is ScannerClient.SCAN_FAILURE]
        cannot_comply_scans = [i for i in results if results[i] is ScannerClient.SCAN_CANNOT_COMPLY]
        print('Scans successful: ' + str(len(successful_scans)))
        print('Scans failed: ' + str(len(failed_scans)))
        print('Scans that could not be completed because the server could not comply: ' + str(len(cannot_comply_scans)))
        if len(successful_scans) != 0:
            print('=== START SUCCESSFUL SCANS ===')
            print(_format_port_list(successful_scans), end='')
            print('=== END SUCCESSFUL SCANS ===')
        if len(failed_scans) != 0:
            print('=== START FAILED SCANS ===')
            print(_format_port_list(failed_scans), end='')
            print('=== END FAILED SCANS ===')
        if len(cannot_comply_scans) != 0:
            print('=== START UNPERFORMED SCANS ===')
            print(_format_port_list(cannot_comply_scans), end='')
            print('=== END UNPERFORMED SCANS ===')
    else:
        return False
    return True

def main():
    if not _main():
        usage()

def usage():
    print('''\
    Usage:
python3 portscanner.py server <[address:]port>
python3 portscanner.py client <address>:<port> <ports>

    Specify scan ports with a comma-separated list of single port numbers (eg.
8080) or port ranges (eg. 2000-3000); do not use spaces in the port list. Port
ranges are inclusive. If no server address is specified, the server will listen
on all ports.''')

if __name__ == '__main__':
    main()
