import json

def load_db(filename):
    with open(filename, 'r') as f:
        return json.loads(f.read())

def _parse(ints):
    ranges = []
    for i in ints:
        if ranges == []:
            ranges.append(str(i))
            continue
        if int(ranges[-1].split('-')[-1]) != i - 1:
            ranges.append(str(i))
        else:
            ranges[-1] = ranges[-1].split('-')[0] + '-' + str(i)
    return ranges

def parse(db):
    return _parse(db['opened']), _parse(db['closed'])

def main():
    filename = input('Port database filename: ')
    opened, closed = parse(load_db(filename))
    print('Opened ports:', ','.join(opened))
    print('Closed ports:', ','.join(closed))

if __name__ == '__main__':
    main()
